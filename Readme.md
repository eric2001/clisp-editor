# CLISP Editor
![CLISP Editor](./screenshot.jpg) 

## Description
This program is a simple text editor designed to be used in conjunction with CLISP.  For more information on CLISP, please see [this site](http://clisp.cons.org/).

This project was started when a course I was taking in college required the use of CLISP for certain in class assignments.  This project provides a basic text editor to enter the code into, rather then entering code directly into CLISP from the command line.  The code can then be saved and loaded directly into CLISP using an option under the File menu.

**License:** [GPL v.2](http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)

## Instructions
Install CLISP (place the lisp.exe and lispinit.mem files) into the same folder as clispeditor.exe.  

Run the program, enter your code and press save.  Make sure to save into the same folder as lisp.exe or CLISP may give you an error.  Press File->Execute to start CLISP and load your code into it.

## History
**Version 2.0.0:**
> - Added some extra options to the text editor such as the Edit menu, Print, and New Window.
> - Released on 07 January 2005
>
> Download: [Version 2.0.0 Setup](/uploads/8a8fe18f0bd0697f353d23d42ec82249/clispeditorSetup200.zip) | [Version 2.0.0 Source](/uploads/630c94ccede93743284dcabd054dfb93/clispeditorSource200.zip)

**Version 1.0.0:**
> - Initial Release
> - Released on 18 February 2004
>
