VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form frmCLISPEditor 
   Caption         =   "CLISP Editor"
   ClientHeight    =   5610
   ClientLeft      =   165
   ClientTop       =   735
   ClientWidth     =   7545
   Icon            =   "frmCLISPEditor.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   5610
   ScaleWidth      =   7545
   StartUpPosition =   3  'Windows Default
   Begin MSComDlg.CommonDialog dialogOpenSave 
      Left            =   1440
      Top             =   1320
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      Filter          =   "LISP Files (*.lisp)|*.lisp|Text Files (*.txt)|*.txt|All Files (*.*)|*.*"
   End
   Begin VB.TextBox code 
      Height          =   5655
      Left            =   0
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   0
      Top             =   0
      Width           =   7575
   End
   Begin VB.Menu menuFile 
      Caption         =   "File"
      Begin VB.Menu menuOpen 
         Caption         =   "Open"
         Shortcut        =   ^O
      End
      Begin VB.Menu menuNew 
         Caption         =   "New"
         Shortcut        =   ^N
      End
      Begin VB.Menu menuNewWin 
         Caption         =   "New Window"
      End
      Begin VB.Menu menuHR1 
         Caption         =   "-"
      End
      Begin VB.Menu menuSave 
         Caption         =   "Save"
         Shortcut        =   ^S
      End
      Begin VB.Menu menuSaveAs 
         Caption         =   "Save As..."
      End
      Begin VB.Menu menuPrint 
         Caption         =   "Print"
         Shortcut        =   ^P
      End
      Begin VB.Menu menuHR2 
         Caption         =   "-"
      End
      Begin VB.Menu menuRunCode 
         Caption         =   "Execute"
         Shortcut        =   ^E
      End
      Begin VB.Menu menuHR3 
         Caption         =   "-"
      End
      Begin VB.Menu menuExit 
         Caption         =   "Exit"
         Shortcut        =   ^Q
      End
   End
   Begin VB.Menu menuEdit 
      Caption         =   "Edit"
      Begin VB.Menu menuCopy 
         Caption         =   "Copy"
         Shortcut        =   ^C
      End
      Begin VB.Menu menuCut 
         Caption         =   "Cut"
         Shortcut        =   ^X
      End
      Begin VB.Menu menuPaste 
         Caption         =   "Paste"
         Shortcut        =   ^V
      End
      Begin VB.Menu menuSelectAll 
         Caption         =   "Select All"
         Shortcut        =   ^A
      End
   End
   Begin VB.Menu menuAbout 
      Caption         =   "About"
   End
End
Attribute VB_Name = "frmCLISPEditor"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' Eric Cavaliere
' Project Name:  CLISP Editor
' Project File Name:  cLISPEditor.vbp
' Source Code File Name:  frmCLISPEditor.frm
' Started on 2/18/2004
' Last Updated on 1/7/2005

Private Sub Form_Resize()
  ' Resize the "code" object whenever the window size is changed.
  On Error Resume Next
  code.Width = frmCLISPEditor.Width - 105
  code.Height = frmCLISPEditor.Height - 765
End Sub

Private Sub Form_Unload(Cancel As Integer)
  End
End Sub

Private Sub menuAbout_Click()
  ' Open the "About" window.
  frmAbout.Show
End Sub

Private Sub menuCopy_Click()
    ' Copy the selected text to the clipboard.
    Clipboard.Clear
    Clipboard.SetText code.SelText
End Sub

Private Sub menuCut_Click()
    ' Copy the selected text to the clipboard, delete the original.
    Clipboard.Clear
    Clipboard.SetText code.SelText
    code.SelText = ""
End Sub

Private Sub menuExit_Click()
  ' Quit the program.
  Unload frmCLISPEditor
End Sub

Private Sub menuNew_Click()
  ' Create a new file.
  dialogOpenSave.FileName = ""
  frmCLISPEditor.Caption = "CLISP Editor"
  code.Text = ""
End Sub

Private Sub menuNewWin_Click()
    ' Open up a new instance of the editor.
    Shell App.EXEName + ".exe"
End Sub

Private Sub menuOpen_Click()
  ' Open an existing file.
  Dim tempLine As String
  dialogOpenSave.ShowOpen
  If (dialogOpenSave.FileName <> "") Then
    code.Text = ""
    Open dialogOpenSave.FileName For Input As #1
      While Not EOF(1)
        Line Input #1, tempLine
        code.Text = code.Text + tempLine + vbCrLf
      Wend
    Close #1
    frmCLISPEditor.Caption = "CLISP Editor - " + dialogOpenSave.FileTitle
  End If
End Sub

Private Sub menuPaste_Click()
    ' Paste the text from the clipboard, to the current
    '   position in the code object.
    code.SelText = Clipboard.GetText
End Sub

Private Sub menuPrint_Click()
    ' Send the current contents of "code" to the printer.
    dialogOpenSave.ShowPrinter
    Printer.Print code.Text
End Sub

Private Sub menuRunCode_Click()
  ' Execute clisp, load the current file into it.
  Dim lispCommand As String
  lispCommand = "lisp.exe -B . -M lispinit.mem -i " + dialogOpenSave.FileTitle
  Shell lispCommand
End Sub

Private Sub menuSave_Click()
  ' Save the current file.
  '   If the file has been saved before, use that name, else
  '   prompt the user for a new file name.
  If dialogOpenSave.FileName = "" Then
    dialogOpenSave.ShowSave
  End If
  If dialogOpenSave.FileName <> "" Then
    Open dialogOpenSave.FileName For Output As #1
    Print #1, code.Text
    Close #1
    frmCLISPEditor.Caption = "CLISP Editor - " + dialogOpenSave.FileTitle
  End If
End Sub

Private Sub menuSaveAs_Click()
  ' Save the current file, force the user to select a file name.
  dialogOpenSave.ShowSave
  If dialogOpenSave.FileName <> "" Then
    Open dialogOpenSave.FileName For Output As #1
    Print #1, code.Text
    Close #1
    frmCLISPEditor.Caption = "CLISP Editor - " + dialogOpenSave.FileTitle
  End If
End Sub

Private Sub menuSelectAll_Click()
    ' Select all text in the code object.
    code.SetFocus
    code.SelLength = 0
    code.SelStart = Len(code.Text)
    SendKeys "+^{HOME}"
End Sub
